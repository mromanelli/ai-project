# AI Project
This is the project for Artificial Intelligence course at the University of Padova

## Getting Started
### Prerequisites
LaTeX, Python

### Installing
First, clone this repo:
```
git clone https://gitlab.com/mromanelli/ai-project
```
Then compile the report:
```
cd ai-project/doc
pdflatex relazione.tex
```

## Licenses
Code:
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)] (LICENSE)

PDF files:
[![License: CC BY 4.0](https://licensebuttons.net/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/)  
